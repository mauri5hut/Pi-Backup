#!/bin/bash
################################################################
#
# Webserver Backup (full dd-Backup and dir-based)
# Based on default nginx installation
#
# Syntax: ./webserverBackup.sh -o "full"
#         ./webserverBackup.sh -o "webserver"
#
# Date:         2021-08-28
# Version:      1.0
#
# Changelog:    -
#
################################################################
# Preparations

logFile="/var/log/backup.log"
mailTo="markus@maurischat.com"
piName="pi41"
backupDir="/backup"

#############################
# Functions

webserverBackup() {

    rm -f $backupDir/*.tar.gz
    mkdir $backupDir/$(date +%Y%m%d)_webServerBackup
    mkdir $backupDir/$(date +%Y%m%d)_webServerBackup/nginxConf
    mkdir $backupDir/$(date +%Y%m%d)_webServerBackup/nginxWwwRoot

    cp /etc/nginx/sites-available/* $backupDir/$(date +%Y%m%d)_webServerBackup/nginxConf/
    cp -r /usr/share/nginx/html/* $backupDir/$(date +%Y%m%d)_webServerBackup/nginxWwwRoot/

    tar -cf $backupDir/$(date +%Y%m%d)_webServerBackup.tar -C / ${backupDir:1}/$(date +%Y%m%d)_webServerBackup
    gzip $backupDir/$(date +%Y%m%d)_webServerBackup.tar 

    rm -rf $backupDir/$(date +%Y%m%d)_webServerBackup
}

#############################
# Main

while getopts o: flag
do
    case "${flag}" in
        o) option=${OPTARG};;
    esac
done

if [ -e $logFile ]; then

    LOGFILESIZE=$(wc -c <"$logFile")
    if [ $LOGFILESIZE -gt 5000000 ]; then
        mv $logFile "$logFile.$(date +%Y%m%d)"
        gzip "$logFile.$(date +%Y%m%d)"
    fi
fi

echo "***********************************" >> $logFile
echo `date` " - Starting..." >> $logFile

if mountpoint -q $backupDir; then

    if [ "$option" = "full" ]; then

        # Full backup

        echo "Starting full backup..." >> $logFile

        rm -f $backupDir/*.img

        dd if=/dev/mmcblk0 of=$backupDir/$(date +%Y%m%d)${piName}FullBackup.img

        # Webserver backup

        webserverBackup

    elif [ "$option" = "webserver" ]; then

        echo "Starting webserver backup only..." >> $logFile

        webserverBackup

    else
        echo "Unknown option $option" >> $logFile
    fi
else
    echo "$backupDir not mounted! Abort..." >> $logFile
    echo "$backupDir not mounted!" | mailx -s "Webserver Backup" $mailTo
fi

echo `date` " - End." >> $logFile