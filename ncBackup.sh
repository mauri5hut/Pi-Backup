#!/bin/bash
################################################################
#
# Backup Nexcloud-Pi (full dd-Backup and dir-based)
# Based on default nginx installation
#
# Syntax: ./ncBackup.sh -o "full"
#         ./ncBackup.sh -o "nconly"
#
# Date:         2021-08-28
# Version:      1.0
#
# Changelog:    -
#
################################################################
# Preparations

logFile="/var/log/backup.log"
mailTo="markus@maurischat.com"
piName="pi44"
backupDir="/backup"
ncBasePath="/var/www/nextcloud"
ncNginxConf="nextcloud.conf"
ncDBuser="nextcloud"
ncDBpasswdFile="/root/ncPasswd"
ncDBname="nextcloud"
phpVersion="7.3"

#############################
# Functions

ncBackup() {

    rm -f $backupDir/*.tar.gz
    mkdir $backupDir/$(date +%Y%m%d)_ncBackup
    mkdir $backupDir/$(date +%Y%m%d)_ncBackup/app
    mkdir $backupDir/$(date +%Y%m%d)_ncBackup/nginx
    mkdir $backupDir/$(date +%Y%m%d)_ncBackup/php
    mkdir $backupDir/$(date +%Y%m%d)_ncBackup/db

    cp -r $ncBasePath $backupDir/$(date +%Y%m%d)_ncBackup/app/
    cp /etc/nginx/sites-available/$ncNginxConf $backupDir/$(date +%Y%m%d)_ncBackup/nginx/
    cp -r /etc/php/$phpVersion $backupDir/$(date +%Y%m%d)_ncBackup/php/

    PASSWD=`cat $ncDBpasswdFile`

    mysqldump -u $ncDBuser -p$PASSWD $ncDBname > $backupDir/$(date +%Y%m%d)_ncBackup/db/ncdb.dump

    tar -cf $backupDir/$(date +%Y%m%d)_ncBackup.tar -C / ${backupDir:1}/$(date +%Y%m%d)_ncBackup
    gzip $backupDir/$(date +%Y%m%d)_ncBackup.tar

    rm -rf $backupDir/$(date +%Y%m%d)_ncBackup
}

#############################
# Main

while getopts o: flag
do
    case "${flag}" in
        o) option=${OPTARG};;
    esac
done

if [ -e $logFile ]; then

    LOGFILESIZE=$(wc -c <"$logFile")
    if [ $LOGFILESIZE -gt 5000000 ]; then
        mv $logFile "$logFile.$(date +%Y%m%d)"
        gzip "$logFile.$(date +%Y%m%d)"
    fi
fi

echo "***********************************" >> $logFile
echo `date` " - Starting..." >> $logFile

if mountpoint -q $backupDir; then

    if [ "$option" = "full" ]; then

        # Full backup

        echo "Starting full backup..." >> $logFile

        rm -f $backupDir/*.img

        dd if=/dev/mmcblk0 of=$backupDir/$(date +%Y%m%d)${piName}FullBackup.img

        # Nexcloud backup

        ncBackup

    elif [ "$option" = "nconly" ]; then

        echo "Starting Nextcloud backup only..." >> $logFile

        ncBackup

    else
        echo "Unknown option $option" >> $logFile
    fi
else
    echo "$backupDir not mounted! Abort..." >> $logFile
    echo "$backupDir not mounted!" | mailx -s "Nextcloud Backup" $mailTo
fi

echo `date` " - End." >> $logFile