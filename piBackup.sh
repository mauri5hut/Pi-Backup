#!/bin/bash
################################################################
#
# Collect backups from all Pis
# The loop for backup must be configured individually
# for the Pis (if it is remote, img and/or tar.gz files ...)
#
# Date:         2021-09-16
# Version:      1.2
#
# Changelog:    1.1 - Built in LTB (Long Time Backup).
#                     Cleanup moved to function.
#               1.2 - Oldest file will be changed to LTB 
#                     instead of last file.
#
################################################################
# Preparations

logFile="/var/log/piBackup.log"
logContent=()
mailTo="markus@maurischat.com"
aPis=("pi41" "pi42" "pi43" "pi44")
user="mauri"
remoteBackupDir="/backup"
backupRootDir="/fileserver/piBackups"
fullBackupDir="fullBackups"
webServerBackupDir="webserverBackups"
holdTime="2"
ncHoldTime="5"
longTime="14"

#############################
# Functions

copyLatestFile() {

    while getopts r:u:p:s:f:d: flag
    do
        case "${flag}" in
            r) remote=${OPTARG};;
            u) user=${OPTARG};;
            p) pi=${OPTARG};;
            s) srcFolder=${OPTARG};;
            f) filetype=${OPTARG};;
            d) dstFolder=${OPTARG};;
        esac
    done

    if $remote; then
        latestFile=`ssh $user@$pi "ls -t $srcFolder/*.$filetype | head -1 | rev | cut -d/ -f1 | rev"`
    else
        latestFile=`ls -t $srcFolder/*.$filetype | head -1 | rev | cut -d/ -f1 | rev`
    fi

    if ! [ -z "$latestFile" ]; then

        if ! [ -e "$dstFolder/$latestFile" ]; then

            if $remote; then
                scp $user@$pi:$srcFolder/$latestFile $dstFolder
            else
                cp $srcFolder/$latestFile $dstFolder
            fi

            if [ $? -eq 0 ]; then
                echo "$latestFile copied."
            else
                rm $dstFolder$latestFile
                echo "Error!"
            fi
        else
            echo "$latestFile already copied."
        fi
    else
        echo "Warning! No file found!"
    fi
}

#----------------------------

cleanup() {

    # Cleanup old backups (exclude LTBs)

    backupDir=$1
    copyResult=$2
    holdTime=$3

    if [[ $copyResult == *"copied"* ]]; then

        logContent+="Cleanup...\n"

        filesToDel=`find $backupDir/ -mtime +$holdTime -type f \( ! -iname "LT_*" \)`

        if ! [ -z "$filesToDel" ]; then
            find $backupDir/ -mtime +$holdTime -type f \( ! -iname "LT_*" \) -delete
            logContent+="Deleted $filesToDel\n"
        fi
    fi
}

#----------------------------

handleLTB() {

    backupDir=$1
    copyResult=$2

    if [[ $copyResult == *"copied"* ]]; then

        logContent+="Check LTB...\n"

        LTBfile=`ls -t $backupDir/LT_* | head -1 | rev | cut -d/ -f1 | rev`

        if [ -z "$LTBfile" ]; then
            oldestFile=`ls -t $backupDir/ | sort | head -1 | rev | cut -d/ -f1 | rev`
            mv $backupDir/$oldestFile $backupDir/LT_$oldestFile
            logContent+="Created LTB: LT_$oldestFile\n"
        else

            LTBToDel=`find $backupDir/LT_* -mtime +$longTime`

            if ! [ -z "$LTBToDel" ]; then
                find $backupDir/LT_* -mtime +$longTime -delete
                logContent+="Deleted LTB: $LTBToDel\n"

                oldestFile=`ls -t $backupDir/ | sort | head -1 | rev | cut -d/ -f1 | rev`
                mv $backupDir/$oldestFile $backupDir/LT_$oldestFile
                logContent+="Created LTB: LT_$oldestFile\n"
            fi
        fi
    fi
}

#############################
# Main

if [ -e $logFile ]; then

    LOGFILESIZE=$(wc -c <"$logFile")
    if [ $LOGFILESIZE -gt 5000000 ]; then
        mv $logFile "$logFile.$(date +%Y%m%d)"
        gzip "$logFile.$(date +%Y%m%d)"
    fi
fi

echo "***********************************" >> $logFile
echo `date` " - Starting..." >> $logFile

if ! [ -d $backupRootDir ]; then
    logContent+="Fileserver not mounted! Abort...\n"
    echo "Fileserver not mounted!" | mailx -s "Pi Backup failed" $mailTo
    exit 1
fi

for pi in ${aPis[@]};
do

    # Check if pi is reachable

    ping -c1 -W1 $pi > /dev/null 2>&1
    if [ $? -ne 0 ]; then
        logContent+="$pi is not reachable...\n"
        continue
    fi

    # Copy backup from pi

    logContent+="Copy backup from $pi...\n"

    if [ "$pi" == "pi41" ]; then

        result=`copyLatestFile -r true -u "$user" -p "$pi" -s "$remoteBackupDir" -f "img" -d "$backupRootDir/$pi/$fullBackupDir"`
        logContent+="$result\n"

        cleanup "$backupRootDir/$pi/$fullBackupDir" "$result" "$holdTime"
        handleLTB "$backupRootDir/$pi/$fullBackupDir" "$result"

        result=`copyLatestFile -r true -u "$user" -p "$pi" -s "$remoteBackupDir" -f "tar.gz" -d "$backupRootDir/$pi/$webServerBackupDir"`
        logContent+="$result\n"

        cleanup "$backupRootDir/$pi/$webServerBackupDir" "$result" "$holdTime"
        handleLTB "$backupRootDir/$pi/$webServerBackupDir" "$result"

    elif [ "$pi" == "pi43" ]; then

        result=`copyLatestFile -r false -s "$remoteBackupDir" -f "img" -d "$backupRootDir/$pi/$fullBackupDir"`
        logContent+="$result\n"

        cleanup "$backupRootDir/$pi/$fullBackupDir" "$result" "$holdTime"
        handleLTB "$backupRootDir/$pi/$fullBackupDir" "$result"

    elif [ "$pi" == "pi44" ]; then

        result=`copyLatestFile -r true -u "$user" -p "$pi" -s "$remoteBackupDir" -f "tar.gz" -d "$backupRootDir/$pi/$webServerBackupDir"`
        logContent+="$result\n"

        cleanup "$backupRootDir/$pi/$webServerBackupDir" "$result" "$ncHoldTime"
        handleLTB "$backupRootDir/$pi/$webServerBackupDir" "$result"

    else
        result=`copyLatestFile -r true -u "$user" -p "$pi" -s "$remoteBackupDir" -f "img" -d "$backupRootDir/$pi/$fullBackupDir"`
        logContent+="$result\n"

        cleanup "$backupRootDir/$pi/$fullBackupDir" "$result" "$holdTime"
        handleLTB "$backupRootDir/$pi/$fullBackupDir" "$result"
    fi

    logContent+="------------------------------\n"
done

printf "$logContent" >> $logFile
printf "$logContent" | mailx -s "Pi Backup finished" $mailTo

echo `date` " - End." >> $logFile